# Songs
demo : https://bique14.github.io/dog-play-the-piano/index.html

### Koisuru Fortune Cookie (恋するフォーチュンクッキー)
###### Artist: BNK48
```
intro
XxcBbBbn xcxcB
BbBbn xcxcB
bnbnmnbcB nBcx
cBjx

xxccBB hjjxxj 
xxccBB hjjxxj
jjXXxj XXxcxXx
Bccxx

xxccBB hjjxxj
xxxccBB hjjxxj
jjXXxj XXxxcxXx
Bccxx

mmmmmmnnBmn nnnnbBccx
mmmmmmnnBmnn nnnnbBcxxjx
Bccxxj Bccxxj
cB cB cB cnBc
Bncxxx

cBcxBnBmn
dBcxBBnBm - mnB
jxc jxc
BcBcBcBcBcBcBn
BcBcBcBcBcBcBn
BcBcBcBcBcBcBncxx

```

### Shoichi (初日)
###### Artist: BNK48
```
xxxxzjzx xnbBbj ccBx
xxxxzb jjjjhghhjj
xxxxzjzx xnbBbjccBx
xxxxzb zjhghhggg

cccxc BBBcB xxxzxxzxcmnm
cccxc BBBcB BBBcBBBcBmnmb
nccccnncc BcccBbnm

ความฝันต้องเกิดหยาดเหงื่อจึงได้มา ~
```

### Kimi wa Melody (君はメロディー)
###### Artist: BNK48
```
xxxxzjjz xxxczjh
zzzzjhjzzxzjjhg
jjhgghjjjzjhhgh hhghhghjhzj

xxzjjzxx xcxzzjh
zzjhhjzz zxjjjhg
jjhgghjjjzjhhgh
hgGgg

ggjjxcccBcxjghj cccBcx
jjhjjczjzxc ccxccBbB
cBBbB

ccBbBb bBb
nnbBc BcB BcB
jbBcxxxc cCcBjb gbBvbn
ccB bBb bBb
nnbBcBbnmb
ccBbBcBb ccBbBbmmnbBnbb
```
